function doThing() {
    var bar = document.getElementById("theBar");
    if (width >= 100) { width = 1; setTimeout(function(){doThing();}, 10); } else { width++; bar.style.width = width + '%'; setTimeout(function(){doThing();}, 10); }
}

var endDay = 180,
    maxPart = 119;

var partSelector = document.getElementById("partSelect");
var sessionSelects = document.getElementsByName("sessionSelect");
var bar = document.getElementById("idealProgress");
var aheadCounter = document.getElementById("percentageCounter");
var daysAheadCounter = document.getElementById("daysAheadCounter");

console.log(partSelector);
console.log(partSelector.options[partSelector.selectedIndex]);

function hideAllSessions() {
    for(var i = 0; i < sessionSelects.length; i++){
        sessionSelects[i].style.display = "none";
    }
}

var progress = 0;
partSelector.addEventListener("change", function() {
    var curPart = partSelector.options[partSelector.selectedIndex].value;
    console.log(curPart);
    hideAllSessions();sessionSelects[curPart].style.display = "inline-block";
});

function updateBar(progress) {
    var bar = document.getElementById("theBar");
    bar.style.width = 100*(progress/maxPart) + '%';
    bar.innerHTML = Math.round(100*100*(progress/maxPart))/100 + "%";
    updateProg();
}

function changeFunc(evt) {
    progress = evt.target.value;
    console.log(progress);
    updateBar(progress);
}

for(var i = 0; i < sessionSelects.length; i++){
    sessionSelects[i].addEventListener("change", changeFunc);
}


function updateProg() {
    var curDay = document.getElementById("dayText").value;
    console.log(curDay);
    bar.style.width = 100*(curDay/endDay) + "%";
    var idealProg = 100*(curDay/endDay);
    bar.innerHTML = Math.round(100*100*(curDay/endDay))/100 + "%";
    findDaysAhead();
    if ( progress !== 0 ) {
        var aheadPercent = 100*(((100*(progress/maxPart))-idealProg)/idealProg);
        aheadPercent = Math.round(100*aheadPercent)/100;
        aheadCounter.innerHTML = "Aheadness Percent: " + aheadPercent + "%";
        console.log(aheadPercent); console.log(progress);
        if ( aheadPercent >= 0 ) {
            aheadCounter.style = "color: green;display: inline-block";
        } else { aheadCounter.style = "color: red;display: inline-block"; }
    }
}


//document.getElementById("recalcIdeal").addEventListener("click", function() {
//    var curDay = document.getElementById("dayText").value;
//    console.log(curDay);
//    var bar = document.getElementById("idealProgress");
//    bar.style.width = 100*(curDay/endDay) + "%";
//    var idealProg = 100*(curDay/endDay);
//    bar.innerHTML = Math.round(100*100*(curDay/endDay))/100 + "%";
//    if ( progress != 0 ) {
//        var aheadPercent = 100*(((100*(progress/maxPart))-idealProg)/idealProg);
//        aheadPercent = Math.round(100*aheadPercent)/100
//        var aheadCounter = document.getElementById("percentageCounter");
//        aheadCounter.innerHTML = "Aheadness Percent: " + aheadPercent + "%";
//        console.log(aheadPercent); console.log(progress);
//        if ( aheadPercent > 0 ) {
//            aheadCounter.style = "color: green";
//        } else { aheadCounter.style = "color: red"; }
//    }
//});

document.getElementById("dayText").addEventListener("change", function() {
    var curDay = document.getElementById("dayText").value;
    console.log(curDay);
    bar.style.width = 100*(curDay/endDay) + "%";
    var idealProg = 100*(curDay/endDay);
    bar.innerHTML = Math.round(100*100*(curDay/endDay))/100 + "%";
    findDaysAhead();
    if ( progress !== 0 ) {
        var aheadPercent = 100*(((100*(progress/maxPart))-idealProg)/idealProg);
        aheadPercent = Math.round(100*aheadPercent)/100;
        aheadCounter.innerHTML = "Aheadness Percent: " + aheadPercent + "%";
        console.log(aheadPercent); console.log(progress);
        if ( aheadPercent >= 0 ) {
            aheadCounter.style = "color: green;display: inline-block";
        } else { aheadCounter.style = "color: red;display: inline-block"; }
    }
});

document.getElementById("abridgedModeToggle").addEventListener("change", function() {
    if (this.checked) {
        endDay = 135;
        document.getElementById("dayText").max = 135;
        updateProg();
        findDaysAhead();
    }
    else {
        endDay = 180;
        document.getElementById("dayText").max = 180;
        updateProg();
        findDaysAhead();
   }
});

document.getElementById("q1OnlyToggle").addEventListener("change", function() {
    if (this.checked) {
        maxPart = 27;
        updateBar(progress);
        if(document.getElementById("abridgedModeToggle").checked) {
            document.getElementById("dayText").max = 34;
            endDay = 34;
        } else { document.getElementById("dayText").max = 45;
            endDay = 45;
        }
        updateProg();
        findDaysAhead();
    }
    else {
        maxPart = 119;
        updateBar(progress);
        if(document.getElementById("abridgedModeToggle").checked) {
            document.getElementById("dayText").max = 135;
            endDay = 135;
        } else { document.getElementById("dayText").max = 180;
            endDay = 180;
        }
        updateProg();
        findDaysAhead();
   }
});

function findDaysAhead() {
    if (progress == maxPart ) {
        if (document.getElementById("q1OnlyToggle".checked)) {
            daysAheadCounter.innerHTML = "Section Complete!";
        } else {
            daysAheadCounter.innerHTML = "Course Complete!"
        }
        daysAheadCounter.style = "color: green;display: inline-block";
        return;
    }
    for (var i = 1; i < endDay; i++) {
        var curPercent = Math.round(100*100*(i/endDay))/100;
        console.log(curPercent + "%");
        var idealProg = 100*(i/endDay);
        if ( progress ) {
            var aheadPercent = 100*(((100*(progress/maxPart))-idealProg)/idealProg);
            if ( aheadPercent < 0 ) {
                console.log(i-1);
                var curDay = document.getElementById("dayText").value;
                var daysAhead = (i-1)-curDay;
                console.log(daysAhead);
                daysAheadCounter.innerHTML = "Days Ahead: " + daysAhead;
                if ( daysAhead >= 0 ) {
                    daysAheadCounter.style = "color: green;display: inline-block";
                } else { daysAheadCounter.style = "color: red;display: inline-block"; }
                break;
            }
        } else { break; }
    }
}